package com.cloudaxis.jazz.constants;

import java.util.HashMap;
import java.util.Map;

public class EvaluationMapper {
	
	public static final Map<String, Integer> evaluationTemplateMapper = new HashMap<String, Integer>();
	
	public static final Map<String, Integer> evaluationQuestionMapper = new HashMap<String, Integer>();
	
	
	static{
		evaluationTemplateMapper.put("Caregiver interview evaluation", 1);
		evaluationTemplateMapper.put("Caregiver related positions", 2);
		evaluationTemplateMapper.put("护理人员评价", 3);
		
		evaluationQuestionMapper.put("Understands and agrees with the requirements of being a Live-in Caregiver", 1);
		evaluationQuestionMapper.put("Motivation for being a caregiver", 1);
		evaluationQuestionMapper.put("Interpersonal skills in dealing with clients", 1);
		evaluationQuestionMapper.put("Nursing hands-on experience and knowledge", 1);
		evaluationQuestionMapper.put("Commitment for the job", 1);
		evaluationQuestionMapper.put("Motivation to join Active Global", 2);
		evaluationQuestionMapper.put("Experience & Knowledge in their area", 2);
		evaluationQuestionMapper.put("Interpersonal skills", 2);
		evaluationQuestionMapper.put("What value can they add to our services", 2);
		evaluationQuestionMapper.put("Level of English", 2);
		evaluationQuestionMapper.put("是否充分了解护理人员职位的相关要求", 3);
		evaluationQuestionMapper.put("成为护理人员的动机", 3);
		evaluationQuestionMapper.put("与客户相处时的个人交际能力", 3);
		evaluationQuestionMapper.put("基础护理技能", 3);
		evaluationQuestionMapper.put("母语是否流利", 3);
		
	}

}
