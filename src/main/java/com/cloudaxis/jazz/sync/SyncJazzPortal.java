package com.cloudaxis.jazz.sync;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cloudaxis.jazz.sync.beans.CandicateForJazz;
import com.cloudaxis.jazz.sync.beans.CandicateProfile;
import com.cloudaxis.jazz.sync.beans.EvaluationForJazz;
import com.cloudaxis.jazz.sync.beans.FeedbackForJazz;
import com.cloudaxis.jazz.sync.beans.JazzCandidateOtherFile;
import com.cloudaxis.jazz.sync.beans.JazzCommunications;
import com.cloudaxis.jazz.sync.beans.JazzQuestionnaire;
import com.cloudaxis.jazz.sync.beans.NoteForJazz;
import com.cloudaxis.jazz.sync.service.CandicateForJazzService;
import com.cloudaxis.jazz.sync.service.EvaluationForJazzService;
import com.cloudaxis.jazz.sync.service.FeedbackForJazzService;
import com.cloudaxis.jazz.sync.service.FileUploadService;
import com.cloudaxis.jazz.sync.service.JazzCommunicationsService;
import com.cloudaxis.jazz.sync.service.NoteForJazzService;
import com.cloudaxis.jazz.sync.utils.CsvUtils;
import com.cloudaxis.jazz.sync.utils.PropertieUtils;

public class SyncJazzPortal {
	protected static Logger logger = Logger.getLogger(SyncJazzPortal.class);

	public static void main(String[] args) throws IOException {
		System.out.println("         -----------  start  ----------\r\n    ");
		String candidateFile = new PropertieUtils().getValue("jazz_file");
		String evaluationFile = new PropertieUtils().getValue("evaluation_file");
		String feedbackFile = new PropertieUtils().getValue("feedback_file");
		String noteFile = new PropertieUtils().getValue("note_file");
		
		FileUploadService fileUploadService = new FileUploadService();
		CandicateForJazzService candidateService = new CandicateForJazzService();
		EvaluationForJazzService evaluationService  = new EvaluationForJazzService();
		FeedbackForJazzService feedbackService = new FeedbackForJazzService();
		NoteForJazzService noteService = new NoteForJazzService();
		JazzCommunicationsService jazzCommunicationsService = new JazzCommunicationsService();
		
		System.out.println("         -----------  start candidate ----------\r\n    ");
		logger.debug("start:"+new Date());
		
		//candidate
		List<CandicateForJazz> candicateForJazzs = CsvUtils.getCandicateForJazzs(candidateFile);
		
		//questionnaire
		List<JazzQuestionnaire> jazzQuestionnaireList = CsvUtils.getjazzQuestionnaire(new PropertieUtils().getValue("jazz_questionnaire"));
		
		//communications
		List<JazzCommunications> communicationsList = CsvUtils.jazzCommunications(new PropertieUtils().getValue("jazz_communications"));
		
		//candidates files
		List<JazzCandidateOtherFile> jazzCandidateOtherFiles = CsvUtils.jazzCandidateOtherFile(new PropertieUtils().getValue("jazz_candidate_other_file"));
		
		//Organize data
		List<CandicateForJazz> candicateAllData = CsvUtils.sortData(candicateForJazzs,jazzQuestionnaireList,jazzCandidateOtherFiles);
		
		List<FeedbackForJazz> feedbacksForJazz= CsvUtils.getFeedbacksForJazz(feedbackFile);
		List<NoteForJazz> notesForJazz = CsvUtils.getNotesForJazz(noteFile);
		List<EvaluationForJazz> evaluationForJazzs = CsvUtils.getEvaluationForJazzs(evaluationFile);
		for (CandicateForJazz candicateForJazz : candicateAllData) {
			// sync CSV data to database
			CandicateProfile candicateProfile = candidateService.syncCandidateProfile(candicateForJazz);
			
			System.out.println("====================communications start===============");
			for(JazzCommunications j: communicationsList){
				if(StringUtils.isNoneBlank(j.getProspect_id())){
					if(j.getProspect_id().equals(candicateProfile.getProspectId())){
						jazzCommunicationsService.syncJazzCommunications(j,candicateProfile.getUserId());
					}
				}
			}
			System.out.println("====================communications done===============");
			
			
			System.out.println("         ----------- comments start !!!   ----------\r\n     ");
			// candidate_feedbacks  -> comments_history
			for(FeedbackForJazz feedbackForJazz : feedbacksForJazz){
				if(StringUtils.isNoneBlank(feedbackForJazz.getProspect_id()) && candicateForJazz.getProspectId().equals(feedbackForJazz.getProspect_id())){
					System.out.println("         ----------- feedback start !!!   ----------\r\n     ");
					feedbackService.syncFeedback(feedbackForJazz, candicateProfile.getUserId());
					System.out.println("       ------------- feedback end !!! -----------\r\n            ");
					
				}
			}
					
			
			// candidate_notes -> comments_history
			for(NoteForJazz noteForJazz : notesForJazz){
				if(noteForJazz.getProspect_id().equals(candicateProfile.getProspectId())){
					System.out.println("       ------------- note start !!! -----------\r\n            ");
					noteService.syncNote(noteForJazz, candicateProfile.getUserId());
					System.out.println("       ------------- note end !!! -----------\r\n            ");
				}
			}
			
			System.out.println("         ----------- comments Done !!!   ----------\r\n   ");
			
			
			for(EvaluationForJazz evaluationForJazz : evaluationForJazzs){
				if(evaluationForJazz.getProspect_id().equals(candicateProfile.getProspectId())){
					System.out.println("         ----------- evaluation start  ----------\r\n    ");
					evaluationService.syncEvaluation(evaluationForJazz, candicateProfile.getUserId());
					System.out.println("         ----------- evaluation Done !!!   ----------\r\n   ");
				}
			}
			
			// upload file to s3
			fileUploadService.uploadFiles(candicateProfile);
		}
		
		System.out.println("         ----------- candidate Done !!!   ----------");
		logger.debug("done:"+new Date());
		
		System.out.println("         ----------- Done !!!   ----------");
	}

}
