package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class CandicateForJazz {
	@CsvBind
	private String prospectId;
	@CsvBind
	private String prospectFirstName;
	@CsvBind
	private String prospectLastName;
	@CsvBind
	private String prospectEmail;
	@CsvBind
	private String prospectAddress;
	@CsvBind
	private String prospectCity;
	@CsvBind
	private String prospectState;
	@CsvBind
	private String prospectPostal;
	@CsvBind
	private String prospectPhone;
	@CsvBind
	private String prospectLocation;
	@CsvBind
	private String veteranStatus;
	@CsvBind
	private String disabilityStatus;
	@CsvBind
	private String eeoGender;			//What is your Gender?
	@CsvBind
	private String eeoRace;
	@CsvBind
	private String eeoDisability;
	@CsvBind
	private String prospectSalary;
	@CsvBind
	private String prospectLinkedin;
	@CsvBind
	private String prospectStartDate;
	@CsvBind
	private String prospectReferrer;
	@CsvBind
	private String prospectLanguages;
	@CsvBind
	private String prospectCollege;
	@CsvBind
	private String prospectGpa;
	@CsvBind
	private String prospectTwitter;
	@CsvBind
	private String prospectDateApplied;
	@CsvBind
	private String prospectSource;
	@CsvBind
	private String resumeFilename;
	@CsvBind
	private String jobId;
	@CsvBind
	private String prospectStatus;

	private String dob; 				//what is your date of birth?
	private String nationality; 		//What is your country of citizenship?
	private String country_of_birth;	//What is your place of birth? 
	private String nearest_airport; 	//What is the airport nearest to your home town?
	private String marital_status; 		//What is your marital status
	private String year_graduation; 		//Year of Graduation
	private String year_studies; 		//Number of years of studies
	private String certified_cpr; 		//Are you certified for CPR / First Aid?
	private String caregiver_before_exp; 		//Have you worked as a Caregiver before?, if yes, give details (where, when, etc.)
	private String current_job; 		//Current role / job
	private String current_job_time; 		//For how long have you been in this job?
	private String educational_level; 		//What Nursing degree / Diploma did you obtain?
	private String specialties;				//Please let us know if you have experience in the following areas (tick all boxes where you have at least some experience):
	private String interview_time; 		//Please give us the best Days / Times to schedule an interview with you (eg: "Tuesdays and Wednesday mornings")
	private String skype; 			//Please give us your Skype ID if you have one already.
	private String height; 		//What is your height in centimeters. Please just enter numerical value, eg: 168
	private String weight; 		//What is your weight in kg? Please just enter numerical value, e.g.: 57
	private String motivation;			//What is your main motivation to become a Live-In Caregiver?
	private String has_children; 		//Do you have children and how many?
	private String siblings; 		//How many siblings to you have? (example: 1 brother and 2 sisters)
	private String religion; 		//What is your religion?
	private String food_choice; 		//Do you follow a specific diet?
	private String worked_in_sg; 		//Have you worked in Singapore before?
	private String hobbies; 		//What are your hobbies (what do you like to do when you are not working)?
	private String children_name_age; 		//If you have children, please list their names & ages
	private String sg_fin; 		//Have you worked in Singapore before? If yes, kindly provide us more details on when, position, work permit or pass that was used and your work permit or FIN number.
	private String tesda_ncii; 		//FOR FILIPINO CAREGIVERS ONLY: HAVE YOU OBTAINED TESDA NCII Certification?
	private String availability;		/*Assuming we offer you a job, and we have already secured a visa/worked Permit for you, 
										how soon can you come to Singapore? Please be truthful and realistic, depending on your current notice for resignation and personal commitments. Please note that most of our clients are in a rush to get a caregiver and are not ready to wait a month for you. 
										At the same time, if you commit to a certain availability it means you are able to come in this timeframe. This is IMPORTANT.
										*/

	private String work_in_sg;		/*We are currently offering Live-In Caregiver opportunities both in HONG KONG and SINGAPORE. You CAN apply to BOTH locations to maximize your chances of getting a placement quickly. 
									Please tick ALL locations that interest you*/
	private String work_in_hk;
	private String allergies;				//If you have any allergies, please state which one(s)
	private String applicant_status;					/*The job opportunities we are currently offering concern only LIVE AT HOME CAREGIVING - This means that you will have to live at the home of your client, which is very common, well regulated, and safe in Singapore. 
										THIS IS NOT a HOSPITAL JOB ---
										YOU WILL NOT BE ABLE TO BRING YOUR FAMILY TO SINGAPORE
										***ARE YOU INTERESTED IN THIS JOB ??****/
	private String diagnosed_conditions; 				//Do you have or were you ever diagnosed with any of the following conditions? Please check the corresponding boxes.
	private String time_of_sg;				//For how long would you like to work as a Live-In Caregiver with Active Global?
	private String history_of_treatment;		//If you checked any of the boxes, please provide some information about the treatment/ medication you use(d). *** PLEASE NOTE THAT LIES OR OMISSIONS ABOUT YOUR MEDICAL HISTORY ARE A VERY SEVERE OFFENSE IN SINGAPORE, AND WILL NOT BE TOLERATED ***
	private String education;				//Which Nursing School did you go to? (name of school and city)
	
	private String otherFile;
	
	
	public String getVeteranStatus() {
		return veteranStatus;
	}

	public void setVeteranStatus(String veteranStatus) {
		this.veteranStatus = veteranStatus;
	}

	public String getDisabilityStatus() {
		return disabilityStatus;
	}

	public void setDisabilityStatus(String disabilityStatus) {
		this.disabilityStatus = disabilityStatus;
	}

	public String getOtherFile() {
		return otherFile;
	}

	public void setOtherFile(String otherFile) {
		this.otherFile = otherFile;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getHistory_of_treatment() {
		return history_of_treatment;
	}

	public void setHistory_of_treatment(String history_of_treatment) {
		this.history_of_treatment = history_of_treatment;
	}

	public String getTime_of_sg() {
		return time_of_sg;
	}

	public void setTime_of_sg(String time_of_sg) {
		this.time_of_sg = time_of_sg;
	}

	public String getDiagnosed_conditions() {
		return diagnosed_conditions;
	}

	public void setDiagnosed_conditions(String diagnosed_conditions) {
		this.diagnosed_conditions = diagnosed_conditions;
	}

	public String getAllergies() {
		return allergies;
	}

	public String getApplicant_status() {
		return applicant_status;
	}

	public void setApplicant_status(String applicant_status) {
		this.applicant_status = applicant_status;
	}

	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	public String getWork_in_sg() {
		return work_in_sg;
	}

	public void setWork_in_sg(String work_in_sg) {
		this.work_in_sg = work_in_sg;
	}

	public String getWork_in_hk() {
		return work_in_hk;
	}

	public void setWork_in_hk(String work_in_hk) {
		this.work_in_hk = work_in_hk;
	}

	public String getSpecialties() {
		return specialties;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public void setSpecialties(String specialties) {
		this.specialties = specialties;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCertified_cpr() {
		return certified_cpr;
	}

	public void setCertified_cpr(String certified_cpr) {
		this.certified_cpr = certified_cpr;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSiblings() {
		return siblings;
	}

	public void setSiblings(String siblings) {
		this.siblings = siblings;
	}

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getProspectFirstName() {
		return prospectFirstName;
	}

	public void setProspectFirstName(String prospectFirstName) {
		this.prospectFirstName = prospectFirstName;
	}

	public String getProspectLastName() {
		return prospectLastName;
	}

	public void setProspectLastName(String prospectLastName) {
		this.prospectLastName = prospectLastName;
	}

	public String getProspectEmail() {
		return prospectEmail;
	}

	public void setProspectEmail(String prospectEmail) {
		this.prospectEmail = prospectEmail;
	}

	public String getProspectAddress() {
		return prospectAddress;
	}

	public void setProspectAddress(String prospectAddress) {
		this.prospectAddress = prospectAddress;
	}

	public String getProspectCity() {
		return prospectCity;
	}

	public void setProspectCity(String prospectCity) {
		this.prospectCity = prospectCity;
	}

	public String getProspectState() {
		return prospectState;
	}

	public void setProspectState(String prospectState) {
		this.prospectState = prospectState;
	}

	public String getProspectPostal() {
		return prospectPostal;
	}

	public void setProspectPostal(String prospectPostal) {
		this.prospectPostal = prospectPostal;
	}

	public String getProspectPhone() {
		return prospectPhone;
	}

	public void setProspectPhone(String prospectPhone) {
		this.prospectPhone = prospectPhone;
	}

	public String getProspectLocation() {
		return prospectLocation;
	}

	public void setProspectLocation(String prospectLocation) {
		this.prospectLocation = prospectLocation;
	}

	public String getEeoGender() {
		return eeoGender;
	}

	public void setEeoGender(String eeoGender) {
		this.eeoGender = eeoGender;
	}

	public String getEeoRace() {
		return eeoRace;
	}

	public void setEeoRace(String eeoRace) {
		this.eeoRace = eeoRace;
	}

	public String getEeoDisability() {
		return eeoDisability;
	}

	public void setEeoDisability(String eeoDisability) {
		this.eeoDisability = eeoDisability;
	}

	public String getProspectSalary() {
		return prospectSalary;
	}

	public void setProspectSalary(String prospectSalary) {
		this.prospectSalary = prospectSalary;
	}

	public String getProspectLinkedin() {
		return prospectLinkedin;
	}

	public void setProspectLinkedin(String prospectLinkedin) {
		this.prospectLinkedin = prospectLinkedin;
	}

	public String getProspectStartDate() {
		return prospectStartDate;
	}

	public void setProspectStartDate(String prospectStartDate) {
		this.prospectStartDate = prospectStartDate;
	}

	public String getProspectReferrer() {
		return prospectReferrer;
	}

	public void setProspectReferrer(String prospectReferrer) {
		this.prospectReferrer = prospectReferrer;
	}

	public String getProspectLanguages() {
		return prospectLanguages;
	}

	public void setProspectLanguages(String prospectLanguages) {
		this.prospectLanguages = prospectLanguages;
	}

	public String getProspectCollege() {
		return prospectCollege;
	}

	public void setProspectCollege(String prospectCollege) {
		this.prospectCollege = prospectCollege;
	}

	public String getProspectGpa() {
		return prospectGpa;
	}

	public void setProspectGpa(String prospectGpa) {
		this.prospectGpa = prospectGpa;
	}

	public String getProspectTwitter() {
		return prospectTwitter;
	}

	public void setProspectTwitter(String prospectTwitter) {
		this.prospectTwitter = prospectTwitter;
	}

	public String getProspectDateApplied() {
		return prospectDateApplied;
	}

	public void setProspectDateApplied(String prospectDateApplied) {
		this.prospectDateApplied = prospectDateApplied;
	}

	public String getProspectSource() {
		return prospectSource;
	}

	public void setProspectSource(String prospectSource) {
		this.prospectSource = prospectSource;
	}

	public String getResumeFilename() {
		return resumeFilename;
	}

	public void setResumeFilename(String resumeFilename) {
		this.resumeFilename = resumeFilename;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getProspectStatus() {
		return prospectStatus;
	}

	public void setProspectStatus(String prospectStatus) {
		this.prospectStatus = prospectStatus;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCountry_of_birth() {
		return country_of_birth;
	}

	public void setCountry_of_birth(String country_of_birth) {
		this.country_of_birth = country_of_birth;
	}

	public String getNearest_airport() {
		return nearest_airport;
	}

	public void setNearest_airport(String nearest_airport) {
		this.nearest_airport = nearest_airport;
	}

	public String getYear_graduation() {
		return year_graduation;
	}

	public void setYear_graduation(String year_graduation) {
		this.year_graduation = year_graduation;
	}

	public String getYear_studies() {
		return year_studies;
	}

	public void setYear_studies(String year_studies) {
		this.year_studies = year_studies;
	}

	public String getCaregiver_before_exp() {
		return caregiver_before_exp;
	}

	public void setCaregiver_before_exp(String caregiver_before_exp) {
		this.caregiver_before_exp = caregiver_before_exp;
	}

	public String getCurrent_job() {
		return current_job;
	}

	public void setCurrent_job(String current_job) {
		this.current_job = current_job;
	}

	public String getCurrent_job_time() {
		return current_job_time;
	}

	public void setCurrent_job_time(String current_job_time) {
		this.current_job_time = current_job_time;
	}

	public String getEducational_level() {
		return educational_level;
	}

	public void setEducational_level(String educational_level) {
		this.educational_level = educational_level;
	}

	public String getInterview_time() {
		return interview_time;
	}

	public void setInterview_time(String interview_time) {
		this.interview_time = interview_time;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHas_children() {
		return has_children;
	}

	public void setHas_children(String has_children) {
		this.has_children = has_children;
	}

	public String getFood_choice() {
		return food_choice;
	}

	public void setFood_choice(String food_choice) {
		this.food_choice = food_choice;
	}

	public String getWorked_in_sg() {
		return worked_in_sg;
	}

	public void setWorked_in_sg(String worked_in_sg) {
		this.worked_in_sg = worked_in_sg;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getChildren_name_age() {
		return children_name_age;
	}

	public void setChildren_name_age(String children_name_age) {
		this.children_name_age = children_name_age;
	}

	public String getSg_fin() {
		return sg_fin;
	}

	public void setSg_fin(String sg_fin) {
		this.sg_fin = sg_fin;
	}

	public String getTesda_ncii() {
		return tesda_ncii;
	}

	public void setTesda_ncii(String tesda_ncii) {
		this.tesda_ncii = tesda_ncii;
	}

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}


	
}
