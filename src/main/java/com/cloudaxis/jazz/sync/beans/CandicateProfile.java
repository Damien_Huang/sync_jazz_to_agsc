package com.cloudaxis.jazz.sync.beans;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CandicateProfile {

	@JsonProperty("prospect_id")
	public String prospectId;

	@JsonProperty("user_id")
	public String userId;
	
	public String firstName;
	
	public String lastName;
	
	public Date dob;
	
	public String resumeFilename;
	public String otherFile;
	
	
	public String getOtherFile() {
		return otherFile;
	}

	public void setOtherFile(String otherFile) {
		this.otherFile = otherFile;
	}

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getResumeFilename() {
		return resumeFilename;
	}

	public void setResumeFilename(String resumeFilename) {
		this.resumeFilename = resumeFilename;
	}




}
