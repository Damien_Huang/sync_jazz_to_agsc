package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class Dob {
	@CsvBind
	private String prospectId;
	
	@CsvBind
	private String answer;

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
