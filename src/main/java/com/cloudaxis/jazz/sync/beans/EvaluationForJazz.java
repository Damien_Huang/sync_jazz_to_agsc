package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class EvaluationForJazz {
 
	@CsvBind
	private String prospect_id;
	@CsvBind
	private String prospect_first_name;
	@CsvBind
	private String prospect_last_name;
	@CsvBind
	private String evaluation_date;
	@CsvBind
	private String evaluation_name;
	@CsvBind
	private String evaluation_note;
	@CsvBind
	private String skill_name;
	@CsvBind
	private String skill_weight;
	@CsvBind
	private String skill_rating;
	@CsvBind
	private String rating_comment;
	
	
	
	public String getProspect_id() {
		return prospect_id;
	}
	
	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}
	
	public String getProspect_first_name() {
		return prospect_first_name;
	}
	
	public void setProspect_first_name(String prospect_first_name) {
		this.prospect_first_name = prospect_first_name;
	}
	
	public String getProspect_last_name() {
		return prospect_last_name;
	}
	
	public void setProspect_last_name(String prospect_last_name) {
		this.prospect_last_name = prospect_last_name;
	}
	
	public String getEvaluation_date() {
		return evaluation_date;
	}
	
	public void setEvaluation_date(String evaluation_date) {
		this.evaluation_date = evaluation_date;
	}
	
	public String getEvaluation_name() {
		return evaluation_name;
	}
	
	public void setEvaluation_name(String evaluation_name) {
		this.evaluation_name = evaluation_name;
	}
	
	public String getEvaluation_note() {
		return evaluation_note;
	}
	
	public void setEvaluation_note(String evaluation_note) {
		this.evaluation_note = evaluation_note;
	}
	
	public String getSkill_name() {
		return skill_name;
	}
	
	public void setSkill_name(String skill_name) {
		this.skill_name = skill_name;
	}
	
	public String getSkill_weight() {
		return skill_weight;
	}
	
	public void setSkill_weight(String skill_weight) {
		this.skill_weight = skill_weight;
	}
	
	public String getSkill_rating() {
		return skill_rating;
	}
	
	public void setSkill_rating(String skill_rating) {
		this.skill_rating = skill_rating;
	}
	
	public String getRating_comment() {
		return rating_comment;
	}
	
	public void setRating_comment(String rating_comment) {
		this.rating_comment = rating_comment;
	}
	
}
