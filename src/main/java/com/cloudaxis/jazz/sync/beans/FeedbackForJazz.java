package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class FeedbackForJazz {

	@CsvBind
	private String prospect_id;
	
	@CsvBind
	private String prospect_first_name;
	
	@CsvBind
	private String prospect_last_name;
	
	@CsvBind
	private String prospect_email;
	
	@CsvBind
	private String feedback_text;

	public String getProspect_id() {
		return prospect_id;
	}

	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}

	public String getProspect_first_name() {
		return prospect_first_name;
	}

	public void setProspect_first_name(String prospect_first_name) {
		this.prospect_first_name = prospect_first_name;
	}

	public String getProspect_last_name() {
		return prospect_last_name;
	}

	public void setProspect_last_name(String prospect_last_name) {
		this.prospect_last_name = prospect_last_name;
	}

	public String getProspect_email() {
		return prospect_email;
	}

	public void setProspect_email(String prospect_email) {
		this.prospect_email = prospect_email;
	}

	public String getFeedback_text() {
		return feedback_text;
	}

	public void setFeedback_text(String feedback_text) {
		this.feedback_text = feedback_text;
	}
	
}
