package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class JazzCandidateOtherFile {
	@CsvBind
	private String prospect_id;
	@CsvBind
	private String file_name;
	public String getProspect_id() {
		return prospect_id;
	}
	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	
}
