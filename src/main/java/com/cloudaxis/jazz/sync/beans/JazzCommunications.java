package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class JazzCommunications {
	@CsvBind
	private String prospect_id;
	@CsvBind
	private String comm_id;
	@CsvBind
	private String sender_email;
	@CsvBind
	private String recipient_email;
	@CsvBind
	private String cc;
	@CsvBind
	private String bcc;
	@CsvBind
	private String subject;
	@CsvBind
	private String body;
	@CsvBind
	private String sent;
	public String getProspect_id() {
		return prospect_id;
	}
	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}
	public String getComm_id() {
		return comm_id;
	}
	public void setComm_id(String comm_id) {
		this.comm_id = comm_id;
	}
	public String getSender_email() {
		return sender_email;
	}
	public void setSender_email(String sender_email) {
		this.sender_email = sender_email;
	}
	public String getRecipient_email() {
		return recipient_email;
	}
	public void setRecipient_email(String recipient_email) {
		this.recipient_email = recipient_email;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getSent() {
		return sent;
	}
	public void setSent(String sent) {
		this.sent = sent;
	}
	
	
}
