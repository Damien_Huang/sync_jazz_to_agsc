package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class JazzQuestionnaire {
	@CsvBind
	private String prospect_id;
/*	@CsvBind
	private String prospect_first_name;
	@CsvBind
	private String prospect_last_name;
	@CsvBind
	private String prospect_email;
	@CsvBind
	private String job_title;
	@CsvBind
	private String questionnaire_name;*/
	@CsvBind
	private String question;
	@CsvBind
	private String answer;
	public String getProspect_id() {
		return prospect_id;
	}
	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
