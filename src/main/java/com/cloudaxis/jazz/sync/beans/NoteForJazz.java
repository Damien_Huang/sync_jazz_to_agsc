package com.cloudaxis.jazz.sync.beans;

import com.opencsv.bean.CsvBind;

public class NoteForJazz {
	
	@CsvBind
	private String prospect_id;
	
	@CsvBind
	private String prospect_first_name;
	
	@CsvBind
	private String prospect_last_name;
	
	@CsvBind
	private String comment_contents;
	
	@CsvBind
	private String User;

	public String getProspect_id() {
		return prospect_id;
	}

	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}

	public String getProspect_first_name() {
		return prospect_first_name;
	}

	public void setProspect_first_name(String prospect_first_name) {
		this.prospect_first_name = prospect_first_name;
	}

	public String getProspect_last_name() {
		return prospect_last_name;
	}

	public void setProspect_last_name(String prospect_last_name) {
		this.prospect_last_name = prospect_last_name;
	}

	public String getComment_contents() {
		return comment_contents;
	}

	public void setComment_contents(String comment_contents) {
		this.comment_contents = comment_contents;
	}

	public String getUser() {
		return User;
	}

	public void setUser(String user) {
		User = user;
	}
	
}
