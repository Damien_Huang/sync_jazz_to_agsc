package com.cloudaxis.jazz.sync.beans;

public class SortQuestionnaire {
	private String prospectId; 
	private String dob; 				//what is your date of birth?
	private String nationality; 		//What is your country of citizenship?
	private String country_of_birth;	//What is your place of birth? 
	private String nearest_airport; 	//What is the airport nearest to your home town?
	private String marital_status; 		//What is your marital status
	private String year_graduation; 		//Year of Graduation
	private String year_studies; 		//Number of years of studies
	private String certified_cpr; 		//Are you certified for CPR / First Aid?
	private String caregiver_before_exp; 		//Have you worked as a Caregiver before?, if yes, give details (where, when, etc.)
	private String current_job; 		//Current role / job
	private String current_job_time; 		//For how long have you been in this job?
	private String educational_level; 		//What Nursing degree / Diploma did you obtain?
	private String interview_time; 		//Please give us the best Days / Times to schedule an interview with you (eg: "Tuesdays and Wednesday mornings")
	private String skype; 			//Please give us your Skype ID if you have one already.
	private String height; 		//What is your height in centimeters. Please just enter numerical value, eg: 168
	private String weight; 		//What is your weight in kg? Please just enter numerical value, e.g.: 57
	private String has_children; 		//Do you have children and how many?
	private String siblings; 		//How many siblings to you have? (example: 1 brother and 2 sisters)
	private String religion; 		//What is your religion?
	private String food_choice; 		//Do you follow a specific diet?
	private String worked_in_sg; 		//Have you worked in Singapore before?
	private String hobbies; 		//What are your hobbies (what do you like to do when you are not working)?
	private String children_name_age; 		//Do you have children? If yes, please list their names & ages.
	private String sg_fin; 		//Have you worked in Singapore before? If yes, kindly provide us more details on when, position, work permit or pass that was used and your work permit or FIN number.
	private String tesda_ncii; 		//HAVE YOU OBTAINED TESDA NCII Certification? (this is a compulsory requirement to work with us if you are deployed from The Philippines)
	public String getProspectId() {
		return prospectId;
	}
	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCountry_of_birth() {
		return country_of_birth;
	}
	public void setCountry_of_birth(String country_of_birth) {
		this.country_of_birth = country_of_birth;
	}
	public String getNearest_airport() {
		return nearest_airport;
	}
	public void setNearest_airport(String nearest_airport) {
		this.nearest_airport = nearest_airport;
	}
	public String getMarital_status() {
		return marital_status;
	}
	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}
	public String getYear_graduation() {
		return year_graduation;
	}
	public void setYear_graduation(String year_graduation) {
		this.year_graduation = year_graduation;
	}
	public String getYear_studies() {
		return year_studies;
	}
	public void setYear_studies(String year_studies) {
		this.year_studies = year_studies;
	}
	public String getCertified_cpr() {
		return certified_cpr;
	}
	public void setCertified_cpr(String certified_cpr) {
		this.certified_cpr = certified_cpr;
	}
	public String getCaregiver_before_exp() {
		return caregiver_before_exp;
	}
	public void setCaregiver_before_exp(String caregiver_before_exp) {
		this.caregiver_before_exp = caregiver_before_exp;
	}
	public String getCurrent_job() {
		return current_job;
	}
	public void setCurrent_job(String current_job) {
		this.current_job = current_job;
	}
	public String getCurrent_job_time() {
		return current_job_time;
	}
	public void setCurrent_job_time(String current_job_time) {
		this.current_job_time = current_job_time;
	}
	public String getEducational_level() {
		return educational_level;
	}
	public void setEducational_level(String educational_level) {
		this.educational_level = educational_level;
	}
	public String getInterview_time() {
		return interview_time;
	}
	public void setInterview_time(String interview_time) {
		this.interview_time = interview_time;
	}
	public String getSkype() {
		return skype;
	}
	public void setSkype(String skype) {
		this.skype = skype;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getHas_children() {
		return has_children;
	}
	public void setHas_children(String has_children) {
		this.has_children = has_children;
	}
	public String getSiblings() {
		return siblings;
	}
	public void setSiblings(String siblings) {
		this.siblings = siblings;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getFood_choice() {
		return food_choice;
	}
	public void setFood_choice(String food_choice) {
		this.food_choice = food_choice;
	}
	public String getWorked_in_sg() {
		return worked_in_sg;
	}
	public void setWorked_in_sg(String worked_in_sg) {
		this.worked_in_sg = worked_in_sg;
	}
	public String getHobbies() {
		return hobbies;
	}
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}
	public String getChildren_name_age() {
		return children_name_age;
	}
	public void setChildren_name_age(String children_name_age) {
		this.children_name_age = children_name_age;
	}
	public String getSg_fin() {
		return sg_fin;
	}
	public void setSg_fin(String sg_fin) {
		this.sg_fin = sg_fin;
	}
	public String getTesda_ncii() {
		return tesda_ncii;
	}
	public void setTesda_ncii(String tesda_ncii) {
		this.tesda_ncii = tesda_ncii;
	}

	
}
