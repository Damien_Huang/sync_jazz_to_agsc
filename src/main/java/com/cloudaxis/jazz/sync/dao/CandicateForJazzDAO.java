package com.cloudaxis.jazz.sync.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cloudaxis.jazz.sync.beans.CandicateForJazz;
import com.cloudaxis.jazz.sync.beans.CandicateProfile;
import com.cloudaxis.jazz.sync.beans.NoteForJazz;
import com.cloudaxis.jazz.sync.utils.DbConnection;

import test.Questionnaire;

public class CandicateForJazzDAO {

	protected Logger logger = Logger.getLogger(CandicateForJazzDAO.class);
	
	public CandicateProfile queryCandidateProfileByEmail(String fullName) {
		Connection conn = null;
		PreparedStatement queryEmailPS = null;
		ResultSet resultRs = null;

		CandicateProfile candicateProfile = new CandicateProfile();
		// query
		String sql = "select * from candidate_profile where full_name = ? limit 1";
		try {
			conn = DbConnection.getConnection();
			queryEmailPS = conn.prepareStatement(sql);
			queryEmailPS.setString(1, fullName);
			resultRs = queryEmailPS.executeQuery();
			if (resultRs.next()) {
				candicateProfile.setUserId(resultRs.getString("user_id"));
				candicateProfile.setProspectId(resultRs.getString("prospect_id"));
			}
		} catch (Exception e) {
			logger.error("select candidate by fullname:full_name="+fullName,e);
			e.printStackTrace();
		} finally {
			try {
				DbConnection.closed(conn, resultRs, queryEmailPS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return candicateProfile;
	}

	public void updateCandidateProfile(String userId, CandicateForJazz candicateForJazz) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultRs = null;

		// update
		String sql = "update candidate_profile set prospect_id=?,resume=?,other_files=? where user_id = ? ";
		try {
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, candicateForJazz.getProspectId());
			ps.setString(2, candicateForJazz.getResumeFilename());
			ps.setString(3, candicateForJazz.getOtherFile());
			ps.setString(4, userId);
			ps.executeUpdate();
		} catch (Exception e) {
			logger.error("update candidate_profile:user_id="+userId+"pro_id="+candicateForJazz.getProspectId(),e);
			e.printStackTrace();
		} finally {
			try {
				DbConnection.closed(conn, resultRs, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public CandicateProfile queryCandidateProfileByProspectId(String prospectId) {
		Connection conn = null;
		PreparedStatement queryEmailPS = null;
		ResultSet resultRs = null;

		CandicateProfile candicateProfile = new CandicateProfile();
		// query
		String sql = "select * from candidate_profile where prospect_id = ?";
		try {
			conn = DbConnection.getConnection();
			queryEmailPS = conn.prepareStatement(sql);
			queryEmailPS.setString(1, prospectId);
			resultRs = queryEmailPS.executeQuery();
			if (resultRs.next()) {
				candicateProfile.setUserId(resultRs.getString("user_id"));
				candicateProfile.setProspectId(resultRs.getString("prospect_id"));
				candicateProfile.setFirstName(resultRs.getString("first_name"));
				candicateProfile.setLastName(resultRs.getString("last_name"));
				candicateProfile.setResumeFilename(resultRs.getString("resume"));
				candicateProfile.setOtherFile(resultRs.getString("other_files"));
			}
		} catch (Exception e) {
			logger.error("select candidate_profile:pro_id="+prospectId,e);
			e.printStackTrace();
		} finally {
			try {
				DbConnection.closed(conn, resultRs, queryEmailPS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return candicateProfile;
	}

	private DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

	public void insertCandidateProfile(CandicateForJazz candicateForJazz) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultRs = null;

		final String currentAddress = StringUtils.stripToEmpty(candicateForJazz.getProspectAddress()) + StringUtils.stripToEmpty(candicateForJazz.getProspectCity()) + StringUtils.trimToEmpty(candicateForJazz.getProspectState());
		final String status = getStatus(candicateForJazz.getProspectStatus());
		final String tag = getTag(status);
		
		Date dob = new Date();
		int age=0;
		try {
			if(StringUtils.isNoneBlank(candicateForJazz.getDob())){
				dob = fmt.parse(candicateForJazz.getDob());
				age = new Date().getYear() - dob.getYear();
			}
		} catch (ParseException e1) {
			//logger.error("dob:"+candicateForJazz.getDob()+"->prospect_id="+candicateForJazz.getProspectId(),e1);
			e1.printStackTrace();
		}
		
		if(!StringUtils.isNoneBlank(candicateForJazz.getEeoGender())){
			candicateForJazz.setEeoGender("");
		}
		
		final String sql = "insert into candidate_profile values("
				+ "?,?,?,?,?,?,?,?,?,?,"   // 10
				+ "?,?,?,?,?,?,?,?,?,?,"   // 20
				+ "?,?,?,?,?,?,?,?,?,?,"   // 30
				+ "?,?,?,?,?,?,?,?,?,?,"   // 40
				+ "?,?,?,?,?,?,?,?,?,?,"   // 50
				+ "?,?,?,?,?,?,?,?,?,?,"   // 60
				+ "?,?,?,?,?,?,?,?,?,?,"   // 70
				+ "?,?,?,?,?,?,?,?,?,?,"   // 80
				+ "?,?,?,?,?,?,?,?,?,?,"   // 90
				+ "?,?,?,?,?,?,?,?,?,?,?);"; //101
		final Object[] innerO = new Object[] {
				null,
				null,
				candicateForJazz.getProspectEmail(),
				candicateForJazz.getProspectFirstName()+" "+candicateForJazz.getProspectLastName(),
				candicateForJazz.getProspectFirstName(),
				candicateForJazz.getProspectLastName(),
				candicateForJazz.getEeoGender(),
				candicateForJazz.getDob(),
				age,
				candicateForJazz.getCountry_of_birth(), // 10
				candicateForJazz.getHas_children(),
				candicateForJazz.getSiblings(),
				StringUtils.isNoneBlank(candicateForJazz.getProspectLanguages())?candicateForJazz.getProspectLanguages():"Others",
				StringUtils.isNoneBlank(candicateForJazz.getNationality())?candicateForJazz.getNationality():"Others",
				StringUtils.isNoneBlank(candicateForJazz.getEducational_level())?candicateForJazz.getEducational_level():"Bachelor of Sciences in Nursing or higher",
				StringUtils.isNoneBlank(candicateForJazz.getCertified_cpr())?candicateForJazz.getCertified_cpr():"No",
				0,
				StringUtils.isNoneBlank(candicateForJazz.getMarital_status())?candicateForJazz.getMarital_status():"Single",
				StringUtils.isNoneBlank(candicateForJazz.getReligion())?candicateForJazz.getReligion():"Others",
				StringUtils.isNoneBlank(candicateForJazz.getProspectPhone())?candicateForJazz.getProspectPhone():0, // 20
				candicateForJazz.getHeight(),
				candicateForJazz.getWeight(),
				candicateForJazz.getMotivation(),
				null,
				StringUtils.isNoneBlank(candicateForJazz.getEducation())?candicateForJazz.getEducation():"No",
				candicateForJazz.getSpecialties(),
				candicateForJazz.getHobbies(),
				candicateForJazz.getAvailability(),
				StringUtils.isNoneBlank(candicateForJazz.getProspectDateApplied())?candicateForJazz.getProspectDateApplied():new Date(),
				StringUtils.isNoneBlank(candicateForJazz.getWork_in_hk())?candicateForJazz.getWork_in_hk():"No", // 30
				StringUtils.isNoneBlank(candicateForJazz.getWork_in_sg())?candicateForJazz.getWork_in_sg():"No",
				"No",
				candicateForJazz.getProspectSalary(),
				candicateForJazz.getProspectSalary(),
				candicateForJazz.getProspectSalary(),
				StringUtils.isNoneBlank(candicateForJazz.getFood_choice())?candicateForJazz.getFood_choice():"No restrictions",
				null,
				candicateForJazz.getSkype(),
				candicateForJazz.getResumeFilename(),
				tag, // 40
				0,
				null,
				null,
				null,
				null,
				null,
				candicateForJazz.getProspectLocation(),
				null,
				null,
				candicateForJazz.getNearest_airport(), // 50
				currentAddress,
				0,
				null,
				candicateForJazz.getWork_in_sg(),
				candicateForJazz.getAllergies(),
				candicateForJazz.getDiagnosed_conditions(),
				null,
				candicateForJazz.getApplicant_status(),
				candicateForJazz.getProspectDateApplied(),
				candicateForJazz.getChildren_name_age(), // 60
				null,
				candicateForJazz.getYear_graduation(),
				candicateForJazz.getYear_studies(),
				candicateForJazz.getCaregiver_before_exp(),
				candicateForJazz.getSg_fin(),
				candicateForJazz.getCurrent_job(),
				candicateForJazz.getTime_of_sg(),
				candicateForJazz.getCurrent_job_time(),
				candicateForJazz.getHistory_of_treatment(),
				candicateForJazz.getInterview_time(), // 70
				null,
				candicateForJazz.getProspectReferrer(),
				null,
				candicateForJazz.getOtherFile(), 
				new Date(),
				StringUtils.isNoneBlank(status)?status:0,
				candicateForJazz.getTesda_ncii(),
				null,
				null,
				null, //80
				null,		
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,//90
				null, 
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				0,	// 100
				candicateForJazz.getProspectId()
		}; 			
		try {
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			
			for(int i = 0; i < innerO.length; i++){
				ps.setObject(i+1, innerO[i]);
			}
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("insert candidate:"+"->prospect_id="+candicateForJazz.getProspectId(),e);
			e.printStackTrace();
		}finally {
			try {
				DbConnection.closed(conn, resultRs, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			
	}

	private String getStatus(String prospectStatus) {
		String status = StringUtils.trimToEmpty(prospectStatus);
		
		if (!"".equals(status)) {
			if ("New".equals(status)) {
				status = "0";
			} else if ("Scheduling Interview".equals(status)) {
				status = "1";
			} else if ("Shortlisted".equals(status)) {
				status = "2";
			} else if (status.contains("Awaiting Documents") || "111. AWAITING DOCUMENTS".equals(status)) {
				status = "3";
			} else if ("Declined Offer".equals(status) || "Not Qualified".equals(status) || "Over Qualified".equals(status)
					|| "Location".equals(status) || "Hired Elsewhere".equals(status) || "Spam".equals(status) || "Not selected for now".equals(status)
					|| "Not a Fit".equals(status) || "Not interested in the job".equals(status) || " Not Reachable".equals(status)) {
				status = "4";
			} else if (status.contains("On Hold")) {
				status = "5";
			} else if ("Confirmed but not available yet".equals(status) || "To contact later for nurse job".equals(status) 
					|| "Males put on Hold".equals(status) || "Filipinos put on Hold".equals(status)) {
				status = "6";
			} else if ("Ready For Placement".equals(status)) {
				status = "7";
			} else if ("Tagged".equals(status)) {
				status = "8";
			} else if ("Contracted".equals(status)) {
				status = "9";
			} else if ("Interview Scheduled".equals(status) || "Interviewed".equals(status)) {
				status = "10";
			} else if ("Blacklisted".equals(status)) {
				status = "11";
			} else {
				status = "5";
			}

		}
		return status;
	}

	private String getTag(String status) {
		String tag = "0";
		if ("7".equals(status)) {
			tag = "0";
		} else if ("8".equals(status)) {
			tag = "1";
		} else if ("9".equals(status)) {
			tag = "2";
		} else if ("5".equals(status)) {
			tag = "3";
		}
		return tag;
	}

	public CandicateProfile queryCandidateProfileByFullName(String fullName, String dob) {
		Connection conn = null;
		PreparedStatement queryEmailPS = null;
		ResultSet resultRs = null;

		CandicateProfile candicateProfile = new CandicateProfile();
		// query
		String sql = "select * from candidate_profile where full_name = ? and dob = ? limit 1";
		try {
			conn = DbConnection.getConnection();
			queryEmailPS = conn.prepareStatement(sql);
			queryEmailPS.setString(1, fullName);
			queryEmailPS.setString(2, dob);
			resultRs = queryEmailPS.executeQuery();
			if (resultRs.next()) {
				candicateProfile.setUserId(resultRs.getString("user_id"));
				candicateProfile.setProspectId(resultRs.getString("prospect_id"));
			}
		} catch (Exception e) {
			logger.error("select candidate:"+"->full_name="+fullName+"--->date_of_birth="+dob,e);
			e.printStackTrace();
		} finally {
			try {
				DbConnection.closed(conn, resultRs, queryEmailPS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return candicateProfile;
	}

	public void insertQuestion(Questionnaire q) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultRs = null;

		// query
		String sql = "insert into candidate_questionnaires(prospect_id,question,answer) values(?,?,?)";
			try {
				conn = DbConnection.getConnection();
				ps = conn.prepareStatement(sql);
				ps.setString(1, q.getProspect_id());
				ps.setString(2, q.getQuestion());
				ps.setString(3, q.getAnswer());
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("q:"+q.toString());
			}finally {
				try {
					DbConnection.closed(conn, resultRs, ps);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	}
	
	public String getCandidateIdByFirstNameAndLastName(NoteForJazz noteForJazz) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] names = noteForJazz.getUser().split(" ");
		String sql = "select user_id from users where first_name = '" + names[0].trim() + "' and last_name =  '" + names[1].trim() + "' ;";
		String userId = null;
		try{
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			if(rs.next()){
				userId = rs.getString("user_id");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				DbConnection.closed(conn, rs, ps);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return userId;
	}

}
