package com.cloudaxis.jazz.sync.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.cloudaxis.jazz.constants.EvaluationMapper;
import com.cloudaxis.jazz.sync.beans.EvaluationForJazz;
import com.cloudaxis.jazz.sync.utils.DbConnection;

public class EvaluationForJazzDAO {
	
	protected Logger logger = Logger.getLogger(EvaluationForJazzDAO.class);
 
	public void addEvaluation(EvaluationForJazz evaluationForJazz, String userId){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String sql = "insert into evaluation_user(applicationid, evaluatedid, templateid, questionid, selection, comment)"
				+ "values(?, ?, ?, ?, ?, ?)";
		try{
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(userId)); 
			ps.setInt(2, 1827);
			ps.setInt(3, EvaluationMapper.evaluationTemplateMapper.get(evaluationForJazz.getEvaluation_name()));
			ps.setInt(4, EvaluationMapper.evaluationQuestionMapper.get(evaluationForJazz.getSkill_name()));
			ps.setString(5, evaluationForJazz.getSkill_rating());
			ps.setString(6, evaluationForJazz.getRating_comment());
			ps.executeUpdate();
		}catch(Exception e){
			logger.error("insert evaluation_user from jazz_evaluation!----->applicationid="+userId+"----->pro_id="+evaluationForJazz.getProspect_id(),e);
			e.printStackTrace();
		}finally{
			try{
				DbConnection.closed(conn, resultSet, ps);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public void addEvaluationSummay(EvaluationForJazz evaluationForJazz, String userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String sql = "insert into evaluation_user(applicationid, evaluatedid, templateid, questionid, selection, comment)"
				+ "values(?, ?, ?, ?, ?, ?)";
		try{
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(userId)); 
			ps.setInt(2, 1827);
			ps.setInt(3, EvaluationMapper.evaluationTemplateMapper.get(evaluationForJazz.getEvaluation_name()));
			ps.setInt(4, -1);
			ps.setString(5, evaluationForJazz.getSkill_rating());
			ps.setString(6, evaluationForJazz.getEvaluation_note());
			ps.executeUpdate();
		}catch(Exception e){
			logger.error("insert evaluation_user from jazz_evaluation!------->applicationid="+userId+"------>pro_id="+evaluationForJazz.getProspect_id(),e);
			e.printStackTrace();
		}finally{
			try{
				DbConnection.closed(conn, resultSet, ps);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
