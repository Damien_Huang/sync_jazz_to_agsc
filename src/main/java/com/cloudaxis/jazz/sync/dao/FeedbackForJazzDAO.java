package com.cloudaxis.jazz.sync.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cloudaxis.jazz.sync.beans.FeedbackForJazz;
import com.cloudaxis.jazz.sync.utils.DbConnection;

public class FeedbackForJazzDAO {

	protected Logger logger = Logger.getLogger(FeedbackForJazzDAO.class);
	
	public void addComment(FeedbackForJazz feedbackForJazz, String userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet =null;
		String sql = "insert into comments_history(USER_ID, CANDIDATE_ID, COMMENT, CREATE_DATE, UPDATE_DATE) values(?, ?, ?, ?, ?);";
		
		try{
			conn = DbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, 1827);
			ps.setInt(2, Integer.parseInt(userId));
			ps.setString(3, feedbackForJazz.getFeedback_text());
			ps.setDate(4, new java.sql.Date(new Date().getTime()));
			ps.setDate(5, new java.sql.Date(new Date().getTime()));
			ps.executeUpdate();
		}catch(Exception e){
			logger.error("insert comments_history from jazz_feedback!---->candidate_id="+userId+"------>pro_id="+feedbackForJazz.getProspect_id(),e);
			e.printStackTrace();
		}finally{
			try{
				DbConnection.closed(conn, resultSet, ps);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

}
