package com.cloudaxis.jazz.sync.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cloudaxis.jazz.sync.beans.JazzCommunications;
import com.cloudaxis.jazz.sync.utils.DbConnection;

public class JazzCommunicationsDao {

	protected Logger logger	= Logger.getLogger(JazzCommunicationsDao.class);
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy hh:mm");
	
	public void insertJazzCommunications(JazzCommunications j, String userId) {
		Connection conn = null;
		PreparedStatement queryEmailPS = null;
		ResultSet resultRs = null;

		// query
		String sql = "insert into email_history(subject,content,create_date,cc_email,bcc_email,"
				+ "attachment,type,receive_email,sender_email,send_date,"
				+ "sender_name,receive_name,user_id,candidate_id,flag,"
				+ "messageId,del_flag,to_s3_attachment) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		
		int type=0;			//0:send email,  1:receive email
		if(StringUtils.isNoneBlank(j.getRecipient_email()) && j.getRecipient_email().contains("@activeglobalcaregiver.com")){
			type=1;
		}
		Date createDate = new Date();
		try {
			if(StringUtils.isNoneBlank(j.getSent())){
				createDate = sdf.parse(j.getSent());
			}
		} catch (ParseException e1) {
			//logger.error("dateChange:\n"+"pro_id="+j.getProspect_id()+"----candidate_id="+userId,e1);
			e1.printStackTrace();
		}
		
		Object[] o = new Object[]{j.getSubject(),j.getBody(),createDate,j.getCc(),j.getBcc(),
				null,type,j.getRecipient_email(),j.getSender_email(),createDate,
				j.getSender_email(),j.getRecipient_email(),"Active Global Specialised Caregivers",userId,null,
				j.getComm_id(),null,null
		};
		
		try {
			conn = DbConnection.getConnection();
			queryEmailPS = conn.prepareStatement(sql);
			for(int i=0; i<o.length; i++){
				queryEmailPS.setObject(i+1, o[i]);
			}
			queryEmailPS.executeUpdate();
		} catch (Exception e) {
			logger.error("communiccations------------->insertEmailHistory:comm_id="+j.getComm_id()+"----pro_id="+j.getProspect_id()+"----candidate_id="+userId,e);
			e.printStackTrace();
		} finally {
			try {
				DbConnection.closed(conn, resultRs, queryEmailPS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

}
