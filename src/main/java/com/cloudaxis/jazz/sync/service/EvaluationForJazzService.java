package com.cloudaxis.jazz.sync.service;

import com.cloudaxis.jazz.sync.beans.EvaluationForJazz;
import com.cloudaxis.jazz.sync.dao.EvaluationForJazzDAO;

public class EvaluationForJazzService {

	public void syncEvaluation(EvaluationForJazz evaluationForJazz, String userId) {
		StringBuilder sb = new StringBuilder();
		EvaluationForJazzDAO dao = new EvaluationForJazzDAO();
		String prospectId = evaluationForJazz.getProspect_id();
		sb.append(">>>>>>>>>>>> sync prospect_id = " + prospectId + " and userId is " + userId + " in the new selector");
		if("Can understand & communicate in English".equals(evaluationForJazz.getSkill_name())){
			dao.addEvaluationSummay(evaluationForJazz, userId);
		}else{
			dao.addEvaluation(evaluationForJazz, userId);
		}
		sb.append("[ Success : user_id = " +  userId).append(" ]");
		System.out.println(sb.toString());
	}

}
