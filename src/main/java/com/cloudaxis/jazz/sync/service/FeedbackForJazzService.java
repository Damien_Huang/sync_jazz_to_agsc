package com.cloudaxis.jazz.sync.service;

import com.cloudaxis.jazz.sync.beans.FeedbackForJazz;
import com.cloudaxis.jazz.sync.dao.FeedbackForJazzDAO;

public class FeedbackForJazzService {

	public void syncFeedback(FeedbackForJazz feedbackForJazz, String userId) {
		StringBuilder sb = new StringBuilder();
		FeedbackForJazzDAO dao = new FeedbackForJazzDAO();
		String prospectId = feedbackForJazz.getProspect_id();
		sb.append(">>>>>>>>>>>  sync prospect_id = " + prospectId + " and userId is " + userId + " in the new selector");
		dao.addComment(feedbackForJazz, userId);
		sb.append("[ Success : user_id = " +  userId).append(" ]");
		System.out.println(sb.toString());
	}

}
