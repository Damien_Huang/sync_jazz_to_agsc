package com.cloudaxis.jazz.sync.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.cloudaxis.jazz.sync.beans.CandicateProfile;
import com.cloudaxis.jazz.sync.utils.PropertieUtils;

public class FileUploadService {

	private final String PIC_PATH;
	private final String RESUME_PATH;
	private final String OTHER_FILE_PATH;
	private final List<String> pdfSuffixes = new ArrayList<String>();;
	protected Logger logger = Logger.getLogger(FileUploadService.class);

	public FileUploadService() {
		PropertieUtils propertieUtils = new PropertieUtils();
		PIC_PATH = propertieUtils.getValue("pic_folder_path");
		RESUME_PATH = propertieUtils.getValue("resume_folder_path");
		OTHER_FILE_PATH = propertieUtils.getValue("other_file_folder_ptah");
		pdfSuffixes.add("PDF");
		pdfSuffixes.add("pdf");
	}

	public void uploadFiles(CandicateProfile candicateProfile) throws IOException {

		AmazonS3 s3client = new AmazonS3Client();
		PropertieUtils propertieUtils = new PropertieUtils();
		String region = propertieUtils.getValue("aws.region");
		String[] regionSuffix = region.split("-");
		String bucketName = propertieUtils.getValue("aws.bucket.name") + "-" + regionSuffix[0];

		Region reg = Region.getRegion(Regions.fromName(region));
		s3client.setRegion(reg);
		
		String s3Path = candicateProfile.getUserId() + "/";
		
		StringBuilder stb = new StringBuilder();
		// upload photo
		try {
		/*stb.append("           >>>start upload photo : ");
		File f = new File(PIC_PATH + prospectId);
		File[] files = f.listFiles();
		for (File file : files) {
			PutObjectRequest req_img_thumbnail = new PutObjectRequest(bucketName, s3Path + file.getName(), file);
			s3client.putObject(req_img_thumbnail);
			stb.append( s3Path + file.getName() + ", " );
		}*/
			
		//other files
		stb.append("\r\n----------------->>>>>>>>start upload other files:");
		File o_file = new File(OTHER_FILE_PATH+candicateProfile.getProspectId());
		File[] other_files = o_file.listFiles();
		if(other_files != null && other_files.length>0){
			for(File file : other_files){
				PutObjectRequest req_img_thumbnail = new PutObjectRequest(bucketName, s3Path +"other_file/"+ file.getName(), file);
				s3client.putObject(req_img_thumbnail);
			}
		}

		// upload resume
		stb.append("\r\n           >>>start upload resume : ");
		File f_resume = new File(RESUME_PATH);
		File[] files_resume = f_resume.listFiles();
		if(files_resume != null && files_resume.length>0){
			for (File file : files_resume) {
				String fileName = file.getName();
				if (fileName.equals(candicateProfile.getResumeFilename())) {
					PutObjectRequest req_img_thumbnail = new PutObjectRequest(bucketName, s3Path +"resume/"+ fileName, file);
					s3client.putObject(req_img_thumbnail);
					stb.append( s3Path + fileName+ "\r\n");
					break;
				}
			}
		}
		
		System.out.println(stb.toString());
		} catch (AmazonServiceException ase) {
			logger.error("uset_id="+candicateProfile.getUserId()+"-------->pro_id="+candicateProfile.getProspectId());
			System.out.println("Caught an AmazonServiceException, which " + "means your request made it " + "to Amazon S3, but was rejected with an error response" + " for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			logger.error("uset_id="+candicateProfile.getUserId()+"-------->pro_id="+candicateProfile.getProspectId());
			System.out.println("Caught an AmazonClientException, which " + "means the client encountered " + "an internal error while trying to " + "communicate with S3, " + "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}

	public String getFileNameWhitoutSuffix(String fileName) {
		int lastIndexOf = fileName.lastIndexOf(".");
		return fileName.substring(0, lastIndexOf);
	}

	public String getFileNameSuffix(String fileName) {
		int lastIndexOf = fileName.lastIndexOf(".");
		return fileName.substring(lastIndexOf + 1, fileName.length());
	}

}
