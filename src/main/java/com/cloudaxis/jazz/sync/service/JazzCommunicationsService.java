package com.cloudaxis.jazz.sync.service;

import org.apache.commons.lang3.StringUtils;

import com.cloudaxis.jazz.sync.beans.CandicateForJazz;
import com.cloudaxis.jazz.sync.beans.CandicateProfile;
import com.cloudaxis.jazz.sync.beans.JazzCommunications;
import com.cloudaxis.jazz.sync.dao.CandicateForJazzDAO;
import com.cloudaxis.jazz.sync.dao.JazzCommunicationsDao;

public class JazzCommunicationsService {
	public CandicateProfile syncCandidateProfile(CandicateForJazz candicateForJazz) {
		StringBuilder stb = new StringBuilder();
		CandicateForJazzDAO dao = new CandicateForJazzDAO();
		String prospectId = candicateForJazz.getProspectId();
		//stb.append(">>>>>>>>>>>> sync prospect_id = " + prospectId);
		// query
		CandicateProfile candidateProfileFullName = dao.queryCandidateProfileByFullName(candicateForJazz.getProspectFirstName()+" "+candicateForJazz.getProspectLastName(),candicateForJazz.getDob());
		
		String userId = candidateProfileFullName.getUserId();
		if(StringUtils.isNoneBlank(userId)){
			// update
			stb.append("  -> update user_id = " +userId);
			dao.updateCandidateProfile(userId,candicateForJazz);
		}else{
			// insert
			stb.append("  -> insert new candidate");
			dao.insertCandidateProfile(candicateForJazz);
		}
		// query result
		CandicateProfile candicateProfile = dao.queryCandidateProfileByProspectId(prospectId);
		String userIdTemp = candicateProfile.getUserId();
		if(userIdTemp == null){
			stb.append(" [ Failure ! ]");
		} else {
			stb.append(" [ Success : user_id = " +  userIdTemp).append(" ]");
		}
		System.out.println(stb.toString());
		return candicateProfile;
	}

	public void syncJazzCommunications(JazzCommunications jazzCommunications, String userId) {
		JazzCommunicationsDao jazzCommunicationsDao = new JazzCommunicationsDao();
		jazzCommunicationsDao.insertJazzCommunications(jazzCommunications,userId);
		
	}
	
}
