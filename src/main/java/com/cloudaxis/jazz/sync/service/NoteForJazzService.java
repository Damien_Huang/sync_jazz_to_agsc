package com.cloudaxis.jazz.sync.service;

import org.apache.commons.lang3.StringUtils;

import com.cloudaxis.jazz.sync.beans.NoteForJazz;
import com.cloudaxis.jazz.sync.dao.CandicateForJazzDAO;
import com.cloudaxis.jazz.sync.dao.NoteForJazzDAO;

public class NoteForJazzService {

	public void syncNote(NoteForJazz noteForJazz, String candidateId) {
		StringBuilder sb = new StringBuilder();
		NoteForJazzDAO noteDao = new NoteForJazzDAO();
		CandicateForJazzDAO candidateDao = new CandicateForJazzDAO();
		String prospectId = noteForJazz.getProspect_id();
		sb.append(">>>>>>>>>>>>>   sync prospect_id = " + prospectId + "and userId is " + candidateId + " in the new selector");
		String userId = candidateDao.getCandidateIdByFirstNameAndLastName(noteForJazz);
		if(StringUtils.isEmpty(userId)){
			userId = "1827";
		}
		noteDao.addComment(noteForJazz, candidateId, userId);
		sb.append("[ Success : user_id = " +  candidateId).append(" ]");
		System.out.println(sb.toString());
	}

}
