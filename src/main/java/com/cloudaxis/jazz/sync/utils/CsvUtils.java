package com.cloudaxis.jazz.sync.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.cloudaxis.jazz.sync.beans.CandicateForJazz;
import com.cloudaxis.jazz.sync.beans.Dob;
import com.cloudaxis.jazz.sync.beans.EvaluationForJazz;
import com.cloudaxis.jazz.sync.beans.FeedbackForJazz;
import com.cloudaxis.jazz.sync.beans.JazzCandidateOtherFile;
import com.cloudaxis.jazz.sync.beans.JazzCommunications;
import com.cloudaxis.jazz.sync.beans.JazzQuestionnaire;
import com.cloudaxis.jazz.sync.beans.NoteForJazz;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

import test.Questionnaire;

public class CsvUtils {

	public static List<CandicateForJazz> getCandicateForJazzs(String file){
		String[] columns = new String[] {"prospectId","prospectFirstName","prospectLastName","prospectEmail","prospectAddress",
				 "prospectCity","prospectState","prospectPostal","prospectPhone","prospectLocation","veteranStatus","disabilityStatus","eeoGender",
				 "eeoRace","eeoDisability","prospectSalary","prospectLinkedin","prospectStartDate","prospectReferrer",
				 "prospectLanguages","prospectCollege","prospectGpa","prospectTwitter","prospectDateApplied",
				 "prospectSource","resumeFilename","jobId","prospectStatus" };

		List<CandicateForJazz> candicateForJazzs = null;
		try {
			candicateForJazzs = parseCsvFileToBeans(file, ',', CandicateForJazz.class, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return candicateForJazzs;
	}
	
	public static List<EvaluationForJazz> getEvaluationForJazzs(String evaluationFile) {
		String[] columns = new String[]{
				"prospect_id", "prospect_first_name", "prospect_last_name", "evaluation_date", "evaluation_name",
				"evaluation_note", "skill_name", "skill_weight", "skill_rating", "rating_comment"
		};
		List<EvaluationForJazz> evaluationForJazzs = null;
		try{
			evaluationForJazzs = parseCsvFileToBeans(evaluationFile, ',', EvaluationForJazz.class, columns);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		return evaluationForJazzs;
	}
	
	public static <T> List<T> parseCsvFileToBeans(final String file, final char fieldDelimiter, final Class<T> beanClass, final String[] columns) throws FileNotFoundException {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(file), fieldDelimiter);
			ColumnPositionMappingStrategy<T> strat = new ColumnPositionMappingStrategy<T>();
			strat.setType(beanClass);
			strat.setColumnMapping(columns);
			CsvToBean<T> csv = new CsvToBean<T>();
			List<T> list = csv.parse(strat, reader);
			list.remove(0);
			return list;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static List<Dob> getDob(String file) {
		String[] columns = new String[] {"prospectId","answer" };

		List<Dob> dob = null;
		try {
			dob = parseCsvFileToBeans(file, ',', Dob.class, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return dob;
	}
	
	public static List<Questionnaire> getQuestionnaire(String file){
		String[] columns = new String[] {"prospect_id","question","answer"};

		List<Questionnaire> questionnaire = null;
		try {
			questionnaire = parseCsvFileToBeans(file, ',', Questionnaire.class, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return questionnaire;
	}

	public static List<JazzQuestionnaire> getjazzQuestionnaire(String file) {
		String[] columns = new String[] {"prospect_id","question","answer"};

		List<JazzQuestionnaire> jazzQuestionnaire = null;
		try {
			jazzQuestionnaire = parseCsvFileToBeans(file, ',', JazzQuestionnaire.class, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return jazzQuestionnaire;
	}

	public static List<CandicateForJazz> sortData(List<CandicateForJazz> candicateForJazzList,List<JazzQuestionnaire> jazzQuestionnaireList, List<JazzCandidateOtherFile> jazzCandidateOtherFiles) {
		DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat yMd = new SimpleDateFormat("yyyy-MM-dd");
		
		for(CandicateForJazz c: candicateForJazzList){
			for(int i=0; i<jazzQuestionnaireList.size(); i++){	
				JazzQuestionnaire j =jazzQuestionnaireList.get(i);
				if(c.getProspectId().equals(j.getProspect_id())){
					if(j.getQuestion().contains("date of birth")){		//dob
						try {
							c.setDob(yMd.format(fmt1.parse(j.getAnswer())));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}else if(j.getQuestion().contains("country of citizenship")){	//nationality
						c.setNationality(j.getAnswer());
					}else if(j.getQuestion().contains("place of birth")){
						c.setCountry_of_birth(j.getAnswer());
					}else if(j.getQuestion().contains("airport nearest")){
						c.setNearest_airport(j.getAnswer());
					}else if(j.getQuestion().equals("What is your marital status")){
						c.setMarital_status(j.getAnswer());
					}else if(j.getQuestion().contains("Year of Graduation")){
						c.setYear_graduation(j.getAnswer());
					}else if(j.getQuestion().contains("years of studies")){
						c.setYear_studies(j.getAnswer());
					}else if(j.getQuestion().contains("certified for")){
						c.setCertified_cpr(j.getAnswer());
					}else if(j.getQuestion().contains("worked as a Caregiver before")){
						c.setCaregiver_before_exp(j.getAnswer());
					}else if(j.getQuestion().contains("Current role")){
						c.setCurrent_job(j.getAnswer());
					}else if(j.getQuestion().contains("For how long have you been in this job")){
						c.setCurrent_job_time(j.getAnswer());
					}else if(j.getQuestion().contains("What Nursing degree")){
						c.setEducational_level(j.getAnswer());
					}else if(j.getQuestion().contains("give us the best Days")){
						c.setInterview_time(j.getAnswer());
					}else if(j.getQuestion().contains("Skype ID")){
						c.setSkype(j.getAnswer());
					}else if(j.getQuestion().contains("height in centimeters")){
						c.setHeight(j.getAnswer());
					}else if(j.getQuestion().contains("weight in kg")){
						c.setWeight(j.getAnswer());
					}else if(j.getQuestion().contains("main motivation")){			
						c.setMotivation(j.getAnswer());
					}else if(j.getQuestion().contains("have children and how many")){
						c.setHas_children(j.getAnswer());
					}else if(j.getQuestion().contains("let us know if")){
						c.setSpecialties(j.getAnswer());
					}else if(j.getQuestion().contains("siblings")){
						c.setSiblings(j.getAnswer());
					}else if(j.getQuestion().contains("religion")){
						c.setReligion(j.getAnswer());
					}else if(j.getQuestion().contains("specific diet")){
						c.setFood_choice(j.getAnswer());
					}else if(j.getQuestion().contains("hobbies")){
						c.setHobbies(j.getAnswer());
					}else if(j.getQuestion().contains("If you have children")){
						c.setChildren_name_age(j.getAnswer());
					}else if(j.getQuestion().contains("FIN number")){
						c.setSg_fin(j.getAnswer());
					}else if(j.getQuestion().contains("FOR FILIPINO CAREGIVERS ONLY")){
						c.setTesda_ncii(j.getAnswer());
					}else if(j.getQuestion().contains("Assuming we offer")){
						c.setAvailability(j.getAnswer());
					}else if(j.getQuestion().contains("Have you worked in Singapore before?")){
						c.setWorked_in_sg(j.getAnswer());
					}else if(j.getQuestion().contains("both in HONG KONG ")){
						if(j.getAnswer().contains("Hong Kong") && j.getAnswer().contains("Singapore")){
							c.setWork_in_hk("Yes");
							c.setWork_in_sg("Yes");
						}else if(j.getAnswer().contains("Singapore")){
							c.setWork_in_sg("Yes");
							c.setWork_in_hk("No");
						}else if(j.getAnswer().contains("Hong Kong")){
							c.setWork_in_sg("No");
							c.setWork_in_hk("Yes");
						}else{
							c.setWork_in_hk("No");
							c.setWork_in_sg("No");
						}
					}else if(j.getQuestion().contains("If you have any allergies")){
						c.setAllergies(j.getAnswer());	
					}else if(j.getQuestion().contains("ARE YOU INTERESTED IN THIS JOB")){
						c.setApplicant_status(j.getAnswer());
					}else if(j.getQuestion().contains("Do you have or were you ever diagnosed")){
						c.setDiagnosed_conditions(j.getAnswer());
					}else if(j.getQuestion().contains("For how long would you like to work as")){
						c.setTime_of_sg(j.getAnswer());
					}else if(j.getQuestion().contains("AND WILL NOT BE TOLERATED")){
						c.setHistory_of_treatment(j.getAnswer());
					}else if(j.getQuestion().contains("What is your Gender?")){
						c.setEeoGender(j.getAnswer());
					}else if(j.getQuestion().contains("Which languages do you speak?")){
						if(StringUtils.isNoneBlank(j.getAnswer())){
							c.setProspectLanguages(j.getAnswer());
						}else{
							c.setProspectLanguages("Others");
						}
					}else if(j.getQuestion().contains("Which Nursing School did you go to?")){
						c.setEducation(j.getAnswer());
					}
					
					
					jazzQuestionnaireList.remove(j);
					i--;
				}
			}
			//--------------
			
			//add other files
			for(JazzCandidateOtherFile jcof : jazzCandidateOtherFiles){
				if(c.getProspectId().equals(jcof.getProspect_id())){
					if(StringUtils.isNoneBlank(jcof.getFile_name())){
						if(StringUtils.isNoneBlank(c.getOtherFile())){
							c.setOtherFile(c.getOtherFile()+","+jcof.getFile_name());
						}else{
							c.setOtherFile(jcof.getFile_name());
						}
					}
				}
			}
			//---------------
		}
		return candicateForJazzList;
	}

	public static List<FeedbackForJazz> getFeedbacksForJazz(String feedbackFile) {
		String[] columns = new String[]{"prospect_id", "prospect_first_name", "prospect_last_name", "prospect_email", "feedback_text"};
		List<FeedbackForJazz> feedbacksForJazz = null;
		try{
			feedbacksForJazz = parseCsvFileToBeans(feedbackFile, ',', FeedbackForJazz.class, columns);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		return feedbacksForJazz;
	}

	public static List<NoteForJazz> getNotesForJazz(String noteFile) {
		String[] columns = new String[]{"prospect_id", "prospect_first_name", "prospect_last_name", "comment_contents", "User"};
		List<NoteForJazz> notesForJazz = null;
		try{
			notesForJazz = parseCsvFileToBeans(noteFile, ',', NoteForJazz.class, columns);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		return notesForJazz;
	}

	public static List<JazzCommunications> jazzCommunications(String file) {
		String[] columns = new String[]{"comm_id", "prospect_id", "sender_email", "recipient_email", "cc", "bcc", "subject", "body","sent"};
		List<JazzCommunications> jazzCommunications = null;
		try{
			jazzCommunications = parseCsvFileToBeans(file, ',', JazzCommunications.class, columns);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		return jazzCommunications;
	}

	public static List<JazzCandidateOtherFile> jazzCandidateOtherFile(String file) {
		String[] columns = new String[]{"prospect_id","file_name"};
		List<JazzCandidateOtherFile> list = null;
		try {
			list = parseCsvFileToBeans(file, ',', JazzCandidateOtherFile.class, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}
}