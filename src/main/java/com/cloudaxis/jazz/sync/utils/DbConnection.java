package com.cloudaxis.jazz.sync.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang3.ArrayUtils;

public class DbConnection {

	static {
		String DRIVERCLASS = new PropertieUtils().getValue("jdbc.driverClassName");
		try {
			Class.forName(DRIVERCLASS);
		} catch (ClassNotFoundException e) {
		}
	}

	public static Connection getConnection() throws SQLException {
		PropertieUtils propertieUtils = new PropertieUtils();
		String USER = propertieUtils.getValue("jdbc.username");
		String PASSWORD = propertieUtils.getValue("jdbc.password");
		String URL = propertieUtils.getValue("jdbc.url");
		return DriverManager.getConnection(URL, USER, PASSWORD);
	}

	public static void closed(Connection conn, ResultSet rs, Statement... ps) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (!ArrayUtils.isEmpty(ps)) {
			for (Statement p : ps)
				if (p != null) {
					p.close();
				}
		}
		if (conn != null) {
			conn.close();
		}

	}
}
