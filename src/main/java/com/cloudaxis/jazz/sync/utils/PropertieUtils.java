package com.cloudaxis.jazz.sync.utils;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertieUtils {
	public Properties propertie;
	public InputStream inputFile;
	public FileOutputStream outputFile;

	public Properties getproperties() {
		propertie = new Properties();
		try {
			inputFile = PropertieUtils.class.getClassLoader().getResourceAsStream("configuration.properties");
			propertie.load(inputFile);
			inputFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return propertie;
	}

	public String getValue(String key) {
		Properties p = new PropertieUtils().getproperties();
		String value = p.getProperty(key);
		if (value == null) {
			value = "";
		}
		return value;
	}

}
