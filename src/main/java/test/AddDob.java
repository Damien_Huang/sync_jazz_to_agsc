package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.junit.Test;

import com.cloudaxis.jazz.sync.beans.CandicateForJazz;
import com.cloudaxis.jazz.sync.beans.Dob;
import com.cloudaxis.jazz.sync.dao.CandicateForJazzDAO;
import com.cloudaxis.jazz.sync.utils.CsvUtils;
import com.cloudaxis.jazz.sync.utils.PropertieUtils;

public class AddDob {
	public static void main(String[] args) throws Exception {
		/*System.out.println("         -----------  start  ----------\r\n    ");
		String candidateFile = new PropertieUtils().getValue("test");
		
		System.out.println("         -----------  start candidate ----------\r\n    ");
		
		StringBuffer nContent = new StringBuffer();
		List<CandicateForJazz> candicateForJazzs = CsvUtils.getCandicateForJazzs(candidateFile);
		
		List<Dob> dob = CsvUtils.getDob(new PropertieUtils().getValue("dob"));
		
		nContent.append("\r\n");
		for(CandicateForJazz c : candicateForJazzs){
			for(Dob d : dob){
				if(d.getProspectId().equals(c.getProspectId())){
					nContent.append(c.toString()).append(","+d.getAnswer()+"\r\n");
					break;
				}
			}
		}
		
		
		FileOutputStream fileOs = new FileOutputStream(new File("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\test.csv"), false);
		fileOs.write(nContent.toString().getBytes());
		fileOs.close();*/
		
		//addCloumn();
		getQuestionnaire();
	}
	
	public static void execelDob() throws Exception{
	   FileInputStream fs=new FileInputStream("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\c1.csv");
	   POIFSFileSystem ps=new POIFSFileSystem(fs);
	   HSSFWorkbook wb=new HSSFWorkbook(ps);
	   HSSFSheet sheet=wb.getSheetAt(0);
	   HSSFRow row=sheet.getRow(0);
	   System.out.println(sheet.getLastRowNum()+"  "+row.getLastCellNum());
	   FileOutputStream out=new FileOutputStream("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\text.csv");
	   row=sheet.createRow((short)(sheet.getLastRowNum()+1));     
	   row.createCell((short)0).setCellValue(22);
	   row.createCell((short)1).setCellValue(11);
	   row.createCell((short)2).setCellValue(11);
	   out.flush();
	   wb.write(out);
	   out.close();
	   System.out.println(row.getPhysicalNumberOfCells()+"  "+row.getLastCellNum());
	}
	
	public static void addCloumn() throws IOException{
		  BufferedReader bufReader = new BufferedReader(new FileReader("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\candidates.csv"));
		  
		  //BufferedReader dob = new BufferedReader(new FileReader("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\c_q_t.csv"));
		  List<Dob> dob = CsvUtils.getDob(new PropertieUtils().getValue("dob"));
		  
		  String lineStr = "";
		  
		  StringBuffer nContent = new StringBuffer();
		  while((lineStr = bufReader.readLine()) != null){
			  nContent.append(lineStr);
			  for(Dob d : dob){
				if(lineStr.contains(d.getProspectId())){
					nContent.append(",\""+d.getAnswer()+"\"");
					break;
				}
			  }
			  
			  nContent.append("\r\n");
		  }
		  bufReader.close();
		  
		  FileOutputStream fileOs = new FileOutputStream(new File("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\test.csv"), false);
		  fileOs.write(nContent.toString().getBytes());
		  fileOs.close();
	 }
	
	public static void t1(){
		try {  
            BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\CloudAxis\\Desktop\\dob_candidate\\c1.csv"));//换成你的文件名 
            reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉 
            String line = null;  
            while((line=reader.readLine())!=null){  
                String item[] = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分 
                  
                String last = item[item.length-1];//这就是你要的数据了 
                //int value = Integer.parseInt(last);//如果是数值，可以转化为数值 
                System.out.println(last);  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
	}
	
	
	public static void getQuestionnaire(){
		/*String candidateFile = new PropertieUtils().getValue("question");
		List<Questionnaire> qList = CsvUtils.getQuestionnaire(candidateFile);*/
		CandicateForJazzDAO dao = new CandicateForJazzDAO();
		System.out.println("begin:!!!!");
		
		BufferedReader reader;
		Questionnaire q = new Questionnaire();
		try {
			reader = new BufferedReader(new FileReader("C:\\Users\\CloudAxis\\Desktop\\questionnaire\\candidate_questionnaires_20170228.csv"));
			reader.readLine();
			String line = null;  
			while((line=reader.readLine())!=null){  
				String item[] = line.split(",");
				if(item.length >= 3){
					q.setProspect_id(item[0]);
					q.setQuestion(item[1]);
					q.setAnswer(item[2]);
				}else if(item.length == 2){
					q.setProspect_id(item[0]);
					q.setQuestion(item[1]);
				}
				dao.insertQuestion(q);
			}  
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		
		/*for(Questionnaire q : qList){
			dao.insertQuestion(q);
		}*/
		System.out.println("end:!!!!");
	}
}
