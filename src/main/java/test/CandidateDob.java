package test;

import com.opencsv.bean.CsvBind;

public class CandidateDob {
	@CsvBind
	private String prospectId;
	@CsvBind
	private String prospectFirstName;
	@CsvBind
	private String prospectLastName;
	@CsvBind
	private String prospectEmail;
	@CsvBind
	private String prospectAddress;
	@CsvBind
	private String prospectCity;
	@CsvBind
	private String prospectState;
	@CsvBind
	private String prospectPostal;
	@CsvBind
	private String prospectPhone;
	@CsvBind
	private String prospectLocation;
	@CsvBind
	private String eeoGender;
	@CsvBind
	private String eeoRace;
	@CsvBind
	private String eeoDisability;
	@CsvBind
	private String prospectSalary;
	@CsvBind
	private String prospectLinkedin;
	@CsvBind
	private String prospectStartDate;
	@CsvBind
	private String prospectReferrer;
	@CsvBind
	private String prospectLanguages;
	@CsvBind
	private String prospectCollege;
	@CsvBind
	private String prospectGpa;
	@CsvBind
	private String prospectTwitter;
	@CsvBind
	private String prospectDateApplied;
	@CsvBind
	private String prospectSource;
	@CsvBind
	private String resumeFilename;
	@CsvBind
	private String jobId;
	@CsvBind
	private String prospectStatus;
	@CsvBind
	private String dob;
	public String getProspectId() {
		return prospectId;
	}
	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}
	public String getProspectFirstName() {
		return prospectFirstName;
	}
	public void setProspectFirstName(String prospectFirstName) {
		this.prospectFirstName = prospectFirstName;
	}
	public String getProspectLastName() {
		return prospectLastName;
	}
	public void setProspectLastName(String prospectLastName) {
		this.prospectLastName = prospectLastName;
	}
	public String getProspectEmail() {
		return prospectEmail;
	}
	public void setProspectEmail(String prospectEmail) {
		this.prospectEmail = prospectEmail;
	}
	public String getProspectAddress() {
		return prospectAddress;
	}
	public void setProspectAddress(String prospectAddress) {
		this.prospectAddress = prospectAddress;
	}
	public String getProspectCity() {
		return prospectCity;
	}
	public void setProspectCity(String prospectCity) {
		this.prospectCity = prospectCity;
	}
	public String getProspectState() {
		return prospectState;
	}
	public void setProspectState(String prospectState) {
		this.prospectState = prospectState;
	}
	public String getProspectPostal() {
		return prospectPostal;
	}
	public void setProspectPostal(String prospectPostal) {
		this.prospectPostal = prospectPostal;
	}
	public String getProspectPhone() {
		return prospectPhone;
	}
	public void setProspectPhone(String prospectPhone) {
		this.prospectPhone = prospectPhone;
	}
	public String getProspectLocation() {
		return prospectLocation;
	}
	public void setProspectLocation(String prospectLocation) {
		this.prospectLocation = prospectLocation;
	}
	public String getEeoGender() {
		return eeoGender;
	}
	public void setEeoGender(String eeoGender) {
		this.eeoGender = eeoGender;
	}
	public String getEeoRace() {
		return eeoRace;
	}
	public void setEeoRace(String eeoRace) {
		this.eeoRace = eeoRace;
	}
	public String getEeoDisability() {
		return eeoDisability;
	}
	public void setEeoDisability(String eeoDisability) {
		this.eeoDisability = eeoDisability;
	}
	public String getProspectSalary() {
		return prospectSalary;
	}
	public void setProspectSalary(String prospectSalary) {
		this.prospectSalary = prospectSalary;
	}
	public String getProspectLinkedin() {
		return prospectLinkedin;
	}
	public void setProspectLinkedin(String prospectLinkedin) {
		this.prospectLinkedin = prospectLinkedin;
	}
	public String getProspectStartDate() {
		return prospectStartDate;
	}
	public void setProspectStartDate(String prospectStartDate) {
		this.prospectStartDate = prospectStartDate;
	}
	public String getProspectReferrer() {
		return prospectReferrer;
	}
	public void setProspectReferrer(String prospectReferrer) {
		this.prospectReferrer = prospectReferrer;
	}
	public String getProspectLanguages() {
		return prospectLanguages;
	}
	public void setProspectLanguages(String prospectLanguages) {
		this.prospectLanguages = prospectLanguages;
	}
	public String getProspectCollege() {
		return prospectCollege;
	}
	public void setProspectCollege(String prospectCollege) {
		this.prospectCollege = prospectCollege;
	}
	public String getProspectGpa() {
		return prospectGpa;
	}
	public void setProspectGpa(String prospectGpa) {
		this.prospectGpa = prospectGpa;
	}
	public String getProspectTwitter() {
		return prospectTwitter;
	}
	public void setProspectTwitter(String prospectTwitter) {
		this.prospectTwitter = prospectTwitter;
	}
	public String getProspectDateApplied() {
		return prospectDateApplied;
	}
	public void setProspectDateApplied(String prospectDateApplied) {
		this.prospectDateApplied = prospectDateApplied;
	}
	public String getProspectSource() {
		return prospectSource;
	}
	public void setProspectSource(String prospectSource) {
		this.prospectSource = prospectSource;
	}
	public String getResumeFilename() {
		return resumeFilename;
	}
	public void setResumeFilename(String resumeFilename) {
		this.resumeFilename = resumeFilename;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getProspectStatus() {
		return prospectStatus;
	}
	public void setProspectStatus(String prospectStatus) {
		this.prospectStatus = prospectStatus;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	@Override
	public String toString() {
		return "CandidateDob [prospectId=" + prospectId + ", prospectFirstName=" + prospectFirstName
				+ ", prospectLastName=" + prospectLastName + ", prospectEmail=" + prospectEmail + ", prospectAddress="
				+ prospectAddress + ", prospectCity=" + prospectCity + ", prospectState=" + prospectState
				+ ", prospectPostal=" + prospectPostal + ", prospectPhone=" + prospectPhone + ", prospectLocation="
				+ prospectLocation + ", eeoGender=" + eeoGender + ", eeoRace=" + eeoRace + ", eeoDisability="
				+ eeoDisability + ", prospectSalary=" + prospectSalary + ", prospectLinkedin=" + prospectLinkedin
				+ ", prospectStartDate=" + prospectStartDate + ", prospectReferrer=" + prospectReferrer
				+ ", prospectLanguages=" + prospectLanguages + ", prospectCollege=" + prospectCollege + ", prospectGpa="
				+ prospectGpa + ", prospectTwitter=" + prospectTwitter + ", prospectDateApplied=" + prospectDateApplied
				+ ", prospectSource=" + prospectSource + ", resumeFilename=" + resumeFilename + ", jobId=" + jobId
				+ ", prospectStatus=" + prospectStatus + ", dob=" + dob + "]";
	}
	
}
