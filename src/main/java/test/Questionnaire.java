package test;

import com.opencsv.bean.CsvBind;

public class Questionnaire {
	@CsvBind
	private String prospect_id;
	@CsvBind
	private String question;
	@CsvBind
	private String answer;
	public String getProspect_id() {
		return prospect_id;
	}
	public void setProspect_id(String prospect_id) {
		this.prospect_id = prospect_id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "Questionnaire [prospect_id=" + prospect_id + ", question=" + question + ", answer=" + answer + "]";
	}
	
}
